package com.example.mike.layoutphotoeditor;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SelectPhotos extends AppCompatActivity implements ItemClickListener {

    private List<Photos>mphotosList=new ArrayList<>();
    private RecyclerView recyclerView;
    private PhotosAdapter mphotosAdapter;
    private RecyclerView.LayoutManager mlayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_photos);

        recyclerView=(RecyclerView)findViewById(R.id.recyclerview);
        mphotosAdapter=new PhotosAdapter(SelectPhotos.this,mphotosList);
        mlayoutManager=new GridLayoutManager(SelectPhotos.this,3);
        recyclerView.setLayoutManager(mlayoutManager);
        recyclerView.setAdapter(mphotosAdapter);

        mphotosAdapter.setupItemClick(this);

        String[] STAR = { "*" };

        Cursor cursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                , STAR, null, null, null);

        if (cursor != null)
        {
            if (cursor.moveToFirst())
            {
                do
                {
                    String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));

                    final BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    options.inJustDecodeBounds = false;
                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                    options.inDither = true;
                    Bitmap bm = BitmapFactory.decodeFile(path,options);

                    mphotosList.add(new Photos(bm));
                    Log.i("Path",path);
                }while (cursor.moveToNext());

            }
            cursor.close();
        }

    }

    @Override
    public void OnItemClicked(View v, int pos) {
        Toast.makeText(getApplicationContext(),"You Clicked : position "+pos,Toast.LENGTH_LONG).show();
    }
}
