package com.example.mike.layoutphotoeditor;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by mc on 12/2/16.
 */

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder> {

    private List<Photos> photosList;
    private Context c;
    private ItemClickListener itemClickListener;

    public void setupItemClick(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public PhotosAdapter(Context c,List<Photos> photosList) {
        this.photosList = photosList;
        this.c=c;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_photo_items,parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Photos p=photosList.get(position);
        holder.mimageView.setImageBitmap(p.getPhotoUri());
    }

    @Override
    public int getItemCount() {
        return photosList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ImageView mimageView;

        public ViewHolder(View itemView) {
            super(itemView);
            mimageView=(ImageView)itemView.findViewById(R.id.photo_Items);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListener != null)
                itemClickListener.OnItemClicked(view, getAdapterPosition());
        }
    }
}
