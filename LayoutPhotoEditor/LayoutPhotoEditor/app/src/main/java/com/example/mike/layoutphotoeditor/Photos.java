package com.example.mike.layoutphotoeditor;

import android.graphics.Bitmap;

/**
 * Created by mc on 12/2/16.
 */

public class Photos {
    Bitmap photoUri;

    public Photos(Bitmap photoUri) {
        this.photoUri = photoUri;
    }

    public Bitmap getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(Bitmap photoUri) {
        this.photoUri = photoUri;
    }
}
