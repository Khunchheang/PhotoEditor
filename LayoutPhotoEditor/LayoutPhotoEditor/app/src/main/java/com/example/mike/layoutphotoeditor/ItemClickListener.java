package com.example.mike.layoutphotoeditor;

import android.view.View;

/**
 * Created by mc on 12/2/16.
 */

public interface ItemClickListener {
    void OnItemClicked(View v,int pos);
}
